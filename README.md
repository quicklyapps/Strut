
### Development/Building ###
To build your own version of Strut you'll need Grunt v0.4.0 or later.


1. Install the latest Grunt: `npm install -g grunt-cli`
2. Clone Strut: `git@gitlab.com:quicklyapps/Strut.git`
3. `cd Strut`
4. Install Strut's development dependencies: `npm install`
5. Run Strut: `grunt server` (the server runs at localhost:9000)

To make a production build of Strut run `grunt build`.
The resulting build will be located in `Strut/dist`.  

### Acknowledgements ###

* ImpressJS https://github.com/bartaz/impress.js/
* Bespoke.js https://github.com/markdalgleish/bespoke.js
* BackboneJS http://documentcloud.github.com/backbone/
* Spectrum https://github.com/bgrins/spectrum
* Etch http://etchjs.com/
* Bootstrap http://twitter.github.io/bootstrap/
* lodash http://lodash.com/
* mousetrap http://craig.is/killing/mice
* RequireJS http://requirejs.org/
* JQuery http://jquery.com/
* HandlebarsJS http://handlebarsjs.com/
* Grunt http://gruntjs.com/
