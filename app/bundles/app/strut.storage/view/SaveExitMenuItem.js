define(['libs/backbone', '../model/ActionHandlers', 'tantaman/web/widgets/ErrorModal', 'lang'],
function(Backbone, ActionHandlers, ErrorModal, lang) {
	return Backbone.View.extend({
		tagName: 'li',
		events: {
			click: 'saveAndExit'
		},

		constructor: function SaveExitMenuItem(modal, model, storageInterface) {
			Backbone.View.prototype.constructor.call(this);
			this.model = model;
			this.saveAsModal = modal;
			this.storageInterface = storageInterface;
		},

		saveAndExit: function() {
			fileName = this.model.fileName();
			if (fileName == null) {
				this.saveAsModal.show(ActionHandlers.save, lang.save_as);
			} else {
				ActionHandlers.saveAndExit(this.storageInterface, this.model, fileName, ErrorModal.show);
			}
		},

		render: function() {
			this.$el.html('<a>' + lang.save_and_exit + '</a>');
			return this;
		}
	});
});