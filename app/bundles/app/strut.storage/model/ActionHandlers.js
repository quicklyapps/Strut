define(
function() {
	return {
		save: function(storageInterface, model, filename, cb) {
			//storageInterface.savePresentation(filename, model.exportPresentation(filename), cb);
			var generators = model.registry
				.getBest('strut.presentation_generator.GeneratorCollection');
			var index = Math.min(window.sessionMeta.generator_index || 0, generators.length - 1);
			var generator = generators[index];
			var previewStr = generator.generate(model.deck());
			var pizarra =  JSON.stringify(model.exportPresentation(filename));
			console.log(pizarra)

			function getUrlParameter(sParam) {
				var sPageURL = decodeURIComponent(window.location.search.substring(1)),
				  sURLVariables = sPageURL.split('&'),
				  sParameterName,
				  i;

				for (i = 0; i < sURLVariables.length; i++) {
				  sParameterName = sURLVariables[i].split('=');
				  if (sParameterName[0] === sParam) {
				      return sParameterName[1] === undefined ? true : sParameterName[1];
				  }
				}
			}

			$.post("/pizarra-multimedia/pizarra_actualizar", {
				id_pizarra: getUrlParameter("id_pizarra"),
				pizarra: pizarra,
				html: previewStr,
				link: "#",
        		guardar: "true"
			}, function(data){
				alert("Se ha guardado la pizarra correctamente!");
	        	console.log(data);
	    	});
			
		},

		saveAndExit: function(storageInterface, model, filename, cb) {
			//storageInterface.savePresentation(filename, model.exportPresentation(filename), cb);
			var generators = model.registry
				.getBest('strut.presentation_generator.GeneratorCollection');
			var index = Math.min(window.sessionMeta.generator_index || 0, generators.length - 1);
			var generator = generators[index];
			var previewStr = generator.generate(model.deck());
			var pizarra =  JSON.stringify(model.exportPresentation(filename));
			console.log(pizarra)

			function getUrlParameter(sParam) {
				var sPageURL = decodeURIComponent(window.location.search.substring(1)),
				  sURLVariables = sPageURL.split('&'),
				  sParameterName,
				  i;

				for (i = 0; i < sURLVariables.length; i++) {
				  sParameterName = sURLVariables[i].split('=');
				  if (sParameterName[0] === sParam) {
				      return sParameterName[1] === undefined ? true : sParameterName[1];
				  }
				}
			}
			
			$.post("/pizarra-multimedia/pizarra_actualizar", {
				id_pizarra: getUrlParameter("id_pizarra"),
				pizarra: pizarra,
				html: previewStr,
				link: "#",
        		guardar: "true"
			}, function(data){
				alert("Se ha guardado la pizarra correctamente!");
	        	location.href="/pizarra-multimedia/pizarras";
	    	});
			
		},

		open: function(storageInterface, model, filename, cb) {
			storageInterface.savePresentation(
				model.fileName(),
				model.exportPresentation(model.fileName()),
				function () {
					storageInterface.load(filename, function(data, err) {
						if (!err) {
							model.importPresentation(data);
						} else {
							console.log(err);
							console.log(err.stack);
						}

						cb(null, err);
					});
				});
		},

		new_: function(model) {
			model.newPresentation();
		}
	};
});