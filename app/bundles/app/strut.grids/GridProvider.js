define(['tantaman/web/widgets/Dropdown',
		'strut/deck/Utils',
		'./GridChooserModal',
		'lang'],
function(View, DeckUtils, GridChooserModal, lang) {
	function GridProvider(opts) {
		var grids = opts.grids;
		var editorModel = opts.editorModel;
		var selector = opts.selector;
		var attr = opts.attr;

		this._view = new View(grids, opts.template,
			{class: 'iconBtns group-dropdown'});
		this._editorModel = editorModel;
		this._selector = selector;
		this._attr = attr;

		this._previewGrid = this._previewGrid.bind(this);
		this._restoreGrid = this._restoreGrid.bind(this);
		this._setGrid = this._setGrid.bind(this);
		this._view.$el.on('mouseover', '.thumbnail', this._previewGrid);
		this._view.$el.on('mouseout', '.thumbnail', this._restoreGrid);
		this._view.$el.on('click', '.thumbnail', this._setGrid);
	}

	
	var GridChooserModal = new GridChooserModal();
	GridChooserModal.render();
	$('#modals').append(GridChooserModal.$el);


	GridProvider.prototype = {
		view: function() {
			return this._view;
		},

		_previewGrid: function(e) {
			var $container = $(this._selector);
			if ($container.length == 0)
				return;


			var grid = {
				cols: $(e.currentTarget).data('cols'),
				rows: $(e.currentTarget).data('rows')
			};
			
			if (grid == null) return;
			if (grid.cols == 'custom' && grid.rows == 'custom') return;

			this._swapGrid($container, grid);
		},

		_setGrid: function(e) {
			var grid = {
				cols: $(e.currentTarget).data('cols'),
				rows: $(e.currentTarget).data('rows')
			};

			var allSlides = $(e.currentTarget).parent().parent().is('.allSlides');

			if (grid.cols == 'custom' && grid.rows == 'custom') {
				var self = this;
				GridChooserModal.show(function(grid) {
					self._setCustomBgColor(allSlides, grid);
				});
				return;
			}

			this._setModelGrid(allSlides,grid);
		},

		_setModelGrid: function(allSlides, grid) {
			if (grid == null)
				return;

			var attr = this._attr.substring(0,1).toLowerCase() + this._attr.substring(1);
			var obj = this._pickObj(allSlides);

			obj.set(attr, grid);
		},

		_pickObj: function(allSlides) {
			if (allSlides) {
				return this._editorModel.deck();
			} else {
				return this._editorModel.activeSlide();
			}
		},

		_restoreGrid: function(e) {
			var allSlides = $(e.currentTarget).parent().parent().is('.allSlides');
			var obj = this._pickObj( allSlides );
			var attr = this._attr.substring(0,1).toLowerCase() + this._attr.substring(1);
			var currentGrid = obj.get(attr);
			var $container = $(this._selector);

			if ($container.length == 0 || typeof(currentGrid))
				return;
		
			this._swapGrid($container, currentGrid);
		},

		_swapGrid: function($el, newGrid) {
			DeckUtils.removeCurrentGrid($el);
			DeckUtils.applyGrid($el,newGrid);
		},

		dispose: function() {
			this._view.dispose();	
		}
	};

	return GridProvider;
});