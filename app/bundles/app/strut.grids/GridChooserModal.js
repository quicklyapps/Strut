define(['libs/backbone'],
function(Backbone) {
	/**
	* A modal that present a grid creator.
	*/
	return Backbone.View.extend({
		className: 'modal hide',
		events: {
			'click .ok': '_apply'
		},

		initialize: function() {
			this._grid = { cols: 0, rows: 0 };
		},

		/**
		* Show the modal.  cb is the
		* function that is called when a grid is chosen.
		* @param {function} cb
		*/
		show: function(cb) {
			//this._cb = cb;
			this.$el.modal('show');
		},

		hide: function() {
			this.$el.modal('hide');
		},

		_apply: function() {
			//this._cb(c);
			this.hide();
		},

		render: function() {
			var self = this;
			this.$el.html(JST['strut.grids/GridChooserModal']());
			this._$body = this.$el.find('.modal-body');
			
			this.$el.modal({
				show: false
			});

			return this;
		},

		constructor: function GridChooserModal() {
			Backbone.View.prototype.constructor.apply(this, arguments);
		}
	});
});