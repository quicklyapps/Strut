define(['./GridProvider',
		'./AvailableGrids'],
function(GridProvider, Grids) {
	'use strict';

	var slideGridProviderFactory = {
		create: function(editorModel) {
			return new GridProvider({
				grids: Grids,
				editorModel: editorModel,
				selector: '.operatingTable .slideContainer',
				attr: 'Grid',
				template: JST['strut.grids/GridChooserDropdown']
			});
		}
	};

	return {
		initialize: function(registry) {
			registry.register({
				interfaces: 'strut.GridProvider',
				meta: {
					modes: {
						'slide-editor': true,
						'overview': true
					},
					overflow: false
				}
			}, slideGridProviderFactory);
		}
	}
});