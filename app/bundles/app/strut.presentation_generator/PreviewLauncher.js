define(function() {
	'use strict';
	var launch = 0;

	function PreviewLauncher(editorModel) {
		this._editorModel = editorModel;
	};

	PreviewLauncher.prototype = {
		launch: function(generator) {
			if (window.previewWind)
				window.previewWind.close();

			this._editorModel.trigger('launch:preview', null);

			var previewStr = generator.generate(this._editorModel.deck());

			localStorage.setItem('preview-string', previewStr);
			localStorage.setItem('preview-config', JSON.stringify({
				surface: this._editorModel.deck().get('surface')
			}));

			window.previewWind = window.open(
				'/pizarra-multimedia/preview_export/' + generator.id + '.html' + generator.getSlideHash(this._editorModel),
				window.location.href);
			var sourceWind = window;
			
			var pizarra = JSON.stringify(this._editorModel.exportPresentation(this._editorModel.fileName()));
			var html = previewStr;
			var link = (localStorage.getItem("link"))?localStorage.getItem("link"):null;

			function getUrlParameter(sParam) {
			  var sPageURL = decodeURIComponent(window.location.search.substring(1)),
			      sURLVariables = sPageURL.split('&'),
			      sParameterName,
			      i;

			  for (i = 0; i < sURLVariables.length; i++) {
			      sParameterName = sURLVariables[i].split('=');
			      if (sParameterName[0] === sParam) {
			          return sParameterName[1] === undefined ? true : sParameterName[1];
			      }
			  }
			}

			$.post("/pizarra-multimedia/pizarra_actualizar", {
				id_pizarra: getUrlParameter("id_pizarra"),
				pizarra: pizarra,
				html: html,
				link: "#"
			}, function(data){
	        	window.previewWind = window.open("/pizarra-multimedia/preview_export/impress.html#/step-1",window.location.href);
	    	});
			
		}
	};

	return PreviewLauncher;
});