define(['libs/backbone'],
function(Backbone) {
	/**
	* A modal that present a flat color chooser.
	*/
	return Backbone.View.extend({
		className: 'modal hide tooltipModal',
		events: {
			'click .ok': '_apply',
			'change .positionForm input[type=radio]': 'setTooltipPosition',
			'change .selectTpEvt': 'setTooltipEvt',
			'change input.typeContent': 'toogleTypeContent'
		},

		/**
		 * @class TooltipModal
		 */
		initialize: function(dataTooltip) {
			this.dataTooltip = dataTooltip;
		},

		show: function(dataTooltip,changeTpProperties) {
			this.dataTooltip = dataTooltip;
			this.changeTpProperties = changeTpProperties;
			this.$el.modal('show');
		},

		hide: function() {
			this.$el.modal('hide');
		},

		_apply: function() {
			var typeContent = this._$body.find('[name=typeContent]:checked').val();
			console.log(typeContent);
			
			if(typeContent == 'text'){
				this.dataTooltip.content.type = 'text';
				this.dataTooltip.content.text = this._$body.find('input.textContent').val();
			}else if(typeContent == 'youtube'){
				this.dataTooltip.content.type = 'youtube';
				this.dataTooltip.content.text = this._getYoutubeId(this._$body.find('input.youtubeContent').val());
			}else if(typeContent == 'web'){
				this.dataTooltip.content.type = 'web';
				this.dataTooltip.content.text = this._$body.find('input.webContent').val();
			}

			this.changeTpProperties(this.dataTooltip);
			this.hide();
		},

		_getYoutubeId: function(url){
			var ID = '';
			url = url.replace(/(>|<)/gi,'').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
			
			if(url[2] !== undefined) {
				ID = url[2].split(/[^0-9a-z_\-]/i);
				ID = ID[0];
			}else {
				ID = url;
			}
			console.log(ID);
			return ID;
		},

		render: function() {
			var self = this;
			var content = (this.dataTooltip.content.type!="youtube") ? this.dataTooltip.content.text : 'https://www.youtube.com/watch?v=' + this.dataTooltip.content.type;
			this.$el.html( JST['strut.slide_components/TooltipModal'](self) );
			this._$body = this.$el.find('.modal-body');
			this._$positionForm = this._$body.find('.positionForm');
			this._$body.find('.typeContent[value="'+ this.dataTooltip.content.type +'"]').prop('checked',true);
			this._$body.find('input.'+ this.dataTooltip.content.type +'Content ').val(content);
			this._$body.find('.well[data-type="'+ this.dataTooltip.content.type +'"]').show();
			this._$positionForm.addClass('bg-pos-'+ this.dataTooltip.position.at.split(' ').join('-'));
			this._$body.find('.positionForm input[value="'+ this.dataTooltip.position.at +'"]').prop('checked',true);
			this._$body.find('input[name=position][value="'+ this.dataTooltip.position.at +'"]').prop('checked',true);
			this._$body.find('.selectTpEvt').val(this.dataTooltip.show.event);
			return this;
		},

		setTooltipPosition: function(e){
			this.
				_$positionForm
				.removeClass('bg-pos-'+ this.dataTooltip.position.at.split(' ').join('-'));
				
			$thisRadio = $(e.target);
				
			this.dataTooltip.position={
				at: $thisRadio.val(),
				my: $thisRadio.data('posmy')
			};

			this.
				_$positionForm
				.addClass('bg-pos-'+ this.dataTooltip.position.at.split(' ').join('-'));
		},

		setTooltipEvt: function(e){
			if(e.target.value == 'mouseenter'){
				this.dataTooltip.show.event = e.target.value;
				this.dataTooltip.hide.event = 'mouseout';
			}else{
				this.dataTooltip.show.event = e.target.value;
				this.dataTooltip.hide.event = e.target.value;
			}
		},

		toogleTypeContent: function(e){
			this._$body.find('.well[data-type]').hide('slow');
			this._$body.find('.well[data-type='+ e.target.value +']').show('slow');
		},

		constructor: function TooltipModal() {
			Backbone.View.prototype.constructor.apply(this, arguments);
		}
	});
});