define(['./ComponentView', './Mixers',
	'tantaman/web/widgets/ManagedColorChooser',
	"tantaman/web/interactions/TouchBridge"],
function(ComponentView, Mixers, ManagedColorChooser,TouchBridge) {
	'use strict';

	var colorChooser = ManagedColorChooser.get('floatingChooser');

	return ComponentView.extend({
		className: 'component shapeView',

		initialize: function() {
			ComponentView.prototype.initialize.apply(this, arguments);
			this.scale = Mixers.scaleObjectEmbed;
			this.model.off('change:scale', this._setUpdatedTransform, this);
			this.model.on('change:scale', Mixers.scaleChangeInlineSvg, this);

			this.model.on('change:fill', this._fillChanged, this);
			this._updateFill = this._updateFill.bind(this);

			this.dblclicked = this.dblclicked.bind(this);
			TouchBridge.on.dblclick(this.$el, this.dblclicked);
		},

		// TODO: update markup on model so fills get preserved?
		// Or maintain outer node here?
		render: function() {
			ComponentView.prototype.render.call(this);
			var obj = this.model.get('markup');
			this.$object = $(obj);

			var scale = this.model.get('scale');

			var $content = this.$el.find('.content');
			var $link = $("<a style='display:inline-flex;vertical-align:top' href='"+ this.model.get('href') +"'></a>");
			$link.append( this.$object );
			$content.append($link);
			//$content.append($('<div class="overlay"></div>'));

			this.$ctrls = this.$el.find('.nav');

			if (scale && scale.width) {
				this.$object.attr(scale);
			} else {
				scale = {
					width: 100,
					height: 100,
					x:1,
					y:1
				};
				this.model.attributes.scale = scale;
				this.$object.attr(scale);
			}

			var fill = this.model.get('fill');
			if (fill)
				this.$object.attr('fill', fill);

			return this.$el;
		},

		_xChanged: function(model, value) {
			ComponentView.prototype._xChanged.apply(this, arguments);
			colorChooser.move({left: this.model.get('x') + this.model.get('scale').width + this.$ctrls.width() + 35 });
		},

		_yChanged: function(model, value) {
			ComponentView.prototype._yChanged.apply(this, arguments);
			colorChooser.move({top: value - 45});
		},

		remove: function() {
			ComponentView.prototype.remove.apply(this, arguments);
			colorChooser.hide({
				move: this._updateFill
			});
		},

		dblclicked: function(e) {
			var linkModel = this.model.get('href');
				
			if(linkModel){
				if(linkModel.indexOf('#/step-')>=0)
					linkModel = linkModel.substr(7);
			}

			var link = prompt("Ingrese el numero de la diapositiva o un enlace externo (escriba 0 para eliminar el enlace)", linkModel);

			if (link != null && link > 0) {
			    this.model.set('href', '#/step-' + link );
			    this.model.set('newTab', false);
			}else if(link && (link.indexOf('http') >= 0 || link.indexOf('https') >= 0)){
				this.model.set('href', link);
				this.model.set('newTab', true);
			}else if(link==0){
				this.model.unset('href');
				this.model.unset('newTab');
			}
		},

		_selectionChanged: function(model, value) {
			var $liColorChooser = this.$el.find('.liColorChooser');
			this.$ctrls = this.$el.find('.nav');

			if (!value) {
				$liColorChooser.css('visibility','visible');
				colorChooser.hide({
					move: this._updateFill
				});
			} else {
				$liColorChooser.css('visibility','hidden');
				colorChooser.show({
					left: this.model.get('x') + this.model.get('scale').width + this.$ctrls.width() + 35,
					top: this.model.get('y') - 45,
					move: this._updateFill,
					appendTo: '.operatingTable > .slideContainer'
				});

				this.$el.parent().find('.sp-preview-inner').parent().parent().parent().css('z-index', this.model.get('z') + 1);
			}

			ComponentView.prototype._selectionChanged.apply(this, arguments);
		},

		_fillChanged: function(model, color) {
			this.$object.attr('fill', color);
		},

		_updateFill: function(color) {
			var shapeColor = color.toRgb();
			var shapeFill = 'rgba('+ shapeColor.r +','+ shapeColor.g +','+ shapeColor.b +','+ shapeColor.a +')';
			this.model.set('fill', shapeFill);
		}
	});
});