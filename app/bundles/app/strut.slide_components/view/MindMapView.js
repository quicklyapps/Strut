define(["./ComponentView"],
	function(ComponentView) {

		/*
	,
		,
		"css!styles/slide_components/joint.css"
		*/

		/**
		 * @class mindMapView
		 * @augments ComponentView
		 */
		return ComponentView.extend({
			className: "component mindMapView",

			/**
			 * Initialize mindMapView component view.
			 */
			initialize: function() {
				return ComponentView.prototype.initialize.apply(this, arguments);
			},

			/**
			 * Render element based on component model.
			 *
			 * @returns {*}
			 */
			render: function() {
				var $frame, scale;
				ComponentView.prototype.render.call(this);
				

				return this.$el;
			},

			


		});
	});