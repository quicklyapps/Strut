define(["./ComponentView"],
	function(ComponentView) {

		/**
		 * @class WebFrameView
		 * @augments ComponentView
		 */
		return ComponentView.extend({
			className: "component geogebraFrameView",

			/**
			 * Initialize WebFrameView component view.
			 */
			initialize: function() {
				return ComponentView.prototype.initialize.apply(this, arguments);
			},

			/**
			 * Render element based on component model.
			 *
			 * @returns {*}
			 */
			render: function() {
				var $frame, scale;
				ComponentView.prototype.render.call(this);

				if( !this.model.get('src') ){
					var srcGeogebra = prompt("Ingrese el código del applet de Geogebra","");
					var defaultSrc = '<iframe scrolling="no" src="https://www.geogebra.org/material/iframe/id/IzNLbFIl/width/700/height/380/border/888888/rc/false/ai/false/sdz/false/smb/false/stb/false/stbh/true/ld/false/sri/true/at/auto" width="700px" height="380px" style="border:0px;"> </iframe>';

					if(!srcGeogebra){
						alert('No ha ingresado ningun codigo, el sistema creará un applet prederterminado');
						srcGeogebra = defaultSrc;
					}

					$frame = $(srcGeogebra);

					if($frame.length == 0 || $frame[0].tagName != "IFRAME" || $frame[0].src.indexOf('geogebra.org') == -1){
						alert("El codigo ingresado no es un applet de geogebra, el sistema cargara uno automaticamente");
						srcGeogebra = defaultSrc;
						$frame = $(srcGeogebra);
					}

					this.model.set('src',$frame[0].src);
					this.model.set('iframeWidth',$frame[0].width);
					this.model.set('iframeHeight',$frame[0].height);
					
				}


				this.$el.find(".content").append($frame);
				this.$el.append('<div class="overlay"></div>');
				scale = this.model.get('scale');
				
				
				this.$el.css({
					width: this.model.get('iframeWidth'),
					height: this.model.get('iframeHeight')
				});
				
				return this.$el;
				
			}
		});
	});