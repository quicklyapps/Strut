define(['libs/backbone',
	'joint',
	"css!styles/slide_components/joint.css",
	"css!styles/slide_components/joint-extension.css"],
function(Backbone, joint) {


	/**
	* A modal that present a flat color chooser.
	*/
	return Backbone.View.extend({
		className: 'modal hide diagramModal',
		events: {
			'click .shape-btn': '_insertShape',
			'keyup .attr-shape': '_changeShapeAttr',
			'change .attr-shape': '_changeShapeAttr',
			'click .ok': '_apply'
		},

		/**
		 * @class TooltipModal
		 */
		initialize: function(diagram) {
			this.diagram = diagram;
		},

		show: function(changeData) {
			var self = this;
			this.changeData = changeData;

			var attr = this.$el.attr('rendered');

			if(typeof attr == "undefined" || attr == false){
				this.$el.attr('rendered',true);

				setTimeout(function () {
					self.initDiagramPlugin(self);
				},100);
			}

			this.$el.modal('show');
		},

		hide: function() {
			this.$el.modal('hide');
		},

		_apply: function() {
			if(this.paper.metalink){
				this.removeShapeClass(this.paper.metalink.source.el,'highlighted');
				delete this.paper.metalink;
			}

			if(this.paper.selectedShape){
				this.removeShapeClass(this.paper.selectedShape.el,'selected-shape');
				delete this.paper.selectedShape;
			}
			
			this.diagram.html = this.paper.$el.html();
			this.diagram.data = this.graph.toJSON();
			this.changeData(this.diagram);
			this.hide();
		},

		initDiagramPlugin: function (self) {

			window.g = require('geometry');


			var graph = new joint.dia.Graph;
			var paper = new joint.dia.Paper({ 
				el: self.$el.find('.diagramContainer'),
				width: 512,
				height: 384,
				gridSize: 1,
				model: graph 
			});


			paper.on('cell:pointerdown', function(cellView, evt, x, y) {
				if(cellView.$box){
					if(paper.selectedShape)
						self.removeShapeClass(paper.selectedShape.el,'selected-shape');	

				    self.addShapeClass(cellView.el,'selected-shape');
				    paper.selectedShape = cellView;

				    self.$el.find('input.attr-shape[data-attr=text]').val( cellView.model.get('attrs').text.text );
				    self.$el.find('input.attr-shape[data-attr=width]').val( cellView.model.get('size').width );
				    self.$el.find('input.attr-shape[data-attr=height]').val( cellView.model.get('size').height );
				    self.$el.find('input.attr-shape[data-attr=color]').val( "#FFFFFF" );
				    self.$el.find('input.attr-shape[data-attr=text-color]').val( "#000000"  );
			    }
			});


			// Create a custom rect.
			// ------------------------

			joint.shapes.html = {};
			joint.shapes.html.Rect = joint.shapes.basic.Generic.extend({
				markup: '<g class="rotatable"><g class="scalable"><rect/></g><text/></g>',
			    defaults: joint.util.deepSupplement({
			        type: 'html.Rect',
			        size: { width: 60, height: 60 },
			        attrs: {
			            'rect': { fill: '#FFFFFF', stroke: 'black', width: 100, height: 60 },
			            'text': { 'font-size': 14, text: '', 'ref-x': .5, 'ref-y': .5, ref: 'rect', 'y-alignment': 'middle', 'x-alignment': 'middle', fill: 'black', 'font-family': 'Arial, helvetica, sans-serif' }
			        }

			    }, joint.shapes.basic.Generic.prototype.defaults)
			});

			// Create a custom ellipse.
			// ------------------------

			joint.shapes.html.Ellipse = joint.shapes.basic.Generic.extend({
				markup: '<g class="rotatable"><g class="scalable"><ellipse/></g><text/></g>',
			    defaults: joint.util.deepSupplement({
			        type: 'html.Ellipse',
			        size: { width: 100, height: 60 },
			        attrs: {
			            'ellipse': { fill: '#FFFFFF', stroke: 'black', rx: 30, ry: 20, transform: 'translate(30, 20)' },
            			'text': { 'font-size': 14, text: '', 'text-anchor': 'middle', 'ref-x': .5, 'ref-y': .5, ref: 'ellipse', 'y-alignment': 'middle', fill: 'black', 'font-family': 'Arial, helvetica, sans-serif' }
			        }
			    }, joint.shapes.basic.Generic.prototype.defaults)
			});

			// Create a custom Rhombus.
			// ------------------------

			joint.shapes.html.Rhombus = joint.shapes.basic.Path.extend({

			    defaults: joint.util.deepSupplement({
			        type: 'html.Rhombus',
			        size: { width: 100, height: 60 },
			        attrs: {
			            'path': { d: 'M 30 0 L 60 30 30 60 0 30 z' },
			            'text': { 'ref-y': .5 }
			        }
			        
			    }, joint.shapes.basic.Path.prototype.defaults)
			});

			self.paper = paper;
			self.graph = graph;

			// Create a custom view for that element that displays an HTML div above it.
			// -------------------------------------------------------------------------

			var ElementViewProperties = {

			    template: [
			        '<div class="html-element">',
			        '<button class="delete">x</button>',
			        '<div class="linker">↔</div>',
			        '</div>'
			    ].join(''),

			    initialize: function() {
			        _.bindAll(this, 'updateBox');
			        joint.dia.ElementView.prototype.initialize.apply(this, arguments);

			        this.$box = $(_.template(this.template)());

			        this.$box.find('.delete').on('click', _.bind(this.model.remove, this.model));
			        this.$box.find('.linker').on('deltadrag', _.bind(this.createLink, this.model));
			        
			        this.$box.find('.linker').on('click', _.bind(this.createLink, this));
			        // Update the box position whenever the underlying model changes.
			        this.model.on('change', this.updateBox, this);
			        // Remove the box when the model gets removed from the graph.
			        this.model.on('remove', this.removeBox, this);

			        this.updateBox();
			    },
			    render: function() {
			        joint.dia.ElementView.prototype.render.apply(this, arguments);
			        this.paper.$el.prepend(this.$box);
			        this.updateBox();
			        return this;
			    },
			    updateBox: function() {
			        // Set the position and dimension of the box so that it covers the JointJS element.
			        var bbox = this.model.getBBox();
			        // Example of updating the HTML with a data stored in the cell model.
			        this.$box.css({ width: bbox.width, height: bbox.height, left: bbox.x, top: bbox.y, transform: 'rotate(' + (this.model.get('angle') || 0) + 'deg)' });
			    },
			    removeBox: function(evt) {
			        this.$box.remove();
			    },
			    createLink: function(evt) {
			    	var _this = this;
			    	
			    	self.addShapeClass(this.el,'highlighted');

			    	if(!this.paper.metalink || this.paper.metalink == null){
			    		this.paper.metalink = {
				    		source: _this,
				    		target: null
				    	};
			    	}

			    	if( this.paper.metalink.source.model.get('id') != _this.model.get('id') ){
			    		this.paper.metalink.target = _this;
			    		var l = new joint.dia.Link({
						    source: { id: _this.paper.metalink.source.model.get('id') },
						    target: { id: _this.paper.metalink.target.model.get('id') },
						    attrs: { '.connection': { 'stroke-width': 5, stroke: '#34495E' } }
						});

						self.removeShapeClass( this.paper.metalink.source.el,'highlighted');
						self.removeShapeClass( this.paper.metalink.target.el,'highlighted');

						delete this.paper.metalink;

						graph.addCells([l]);
			    	}
			    	
			    }
			};

			joint.shapes.html.RectView = joint.dia.ElementView.extend(ElementViewProperties);
			joint.shapes.html.EllipseView = joint.dia.ElementView.extend(ElementViewProperties);
			joint.shapes.html.RhombusView = joint.dia.ElementView.extend(ElementViewProperties);

		
			// Create JointJS elements and add them to the graph as usual.
			// -----------------------------------------------------------
			if(self.diagram.data != null){
				self.graph.fromJSON(self.diagram.data);
			}

		},

		_insertShape: function (e) {
			var el, _this = this , shape = $(e.target).data('shape');

			if( !_this.lastElPosition )
				_this.lastElPosition = { x: _this.paper.$el.width()/2, y: _this.paper.$el.height()/2 };
			else{
				_this.lastElPosition.x += 10;
				_this.lastElPosition.y += 10; 
			}

			var properties = { 
				position: _this.lastElPosition,
				attrs: {
        			'text': { text: 'Texto', 'font-size': 24 }
		        }
			};

			if( shape == "circle" ){
				properties.size = { width: 80, height: 80 };
				el = new joint.shapes.html.Ellipse(properties);
			}else if( shape == "ellipse" )
				el = new joint.shapes.html.Ellipse(properties);
			else if( shape == "rect" ){
				properties.size = { width: 100, height: 70 };
				el = new joint.shapes.html.Rect(properties);
			}else if( shape == "square" )
				el = new joint.shapes.html.Rect(properties);
			else if( shape == "rhombus" )
				el = new joint.shapes.html.Rhombus(properties);

			if(el)
				this.graph.addCells([el]);
		},

		addShapeClass: function (shape,newClass) {
			$(shape).attr('class', function(index, classNames) {
			    return classNames + ' ' + newClass;
			});
		},

		removeShapeClass: function (shape,oldClass) {
			$(shape).attr('class', function(index, classNames) {
			    return classNames.replace(oldClass, '');
			});
		},

		_changeShapeAttr : function (e) {
			if(!this.paper.selectedShape)
				return;

			var attr = $(e.target).data('attr'),
				shape = this.paper.selectedShape.model,
				shapeMarkup;

			log(e.target.value);

			if( shape.attr('rect') )
				shapeMarkup = 'rect';
			else if( shape.attr('path') )
				shapeMarkup = 'path';
			else if( shape.attr('ellipse') )
				shapeMarkup = 'ellipse';

			if(attr == "text")
				shape.attr('text/text', e.target.value );
			else if(attr == "width")
				shape.set('size',{ width : e.target.value, height: shape.get('size').height });
			else if(attr == "height")
				shape.set('size',{ width: shape.get('size').width, height : e.target.value });
			else if(attr == "color")
				shape.attr(shapeMarkup + '/fill', e.target.value );
			else if(attr == "text-color")
				shape.attr('text/fill', e.target.value );
		},

		render: function() {
			var self = this;
			this.$el.html( JST['strut.slide_components/DiagramModal']() );
			this._$body = this.$el.find('.modal-body');

			return this;
		},



		constructor: function DiagramModal() {
			Backbone.View.prototype.constructor.apply(this, arguments);
		}
	});
});