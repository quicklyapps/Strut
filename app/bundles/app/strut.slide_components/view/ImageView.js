define(["./ComponentView",
			'./Mixers',
			"tantaman/web/interactions/TouchBridge"],
	function(ComponentView, Mixers, TouchBridge) {

		/**
		 * @class ImageView
		 * @augments ComponentView
		 */
		return ComponentView.extend({
			className: "component imageView",
			tagName: "div",

			events: function() {
				var myEvents, parentEvents;
				parentEvents = ComponentView.prototype.events();
				myEvents = {
					"editComplete": "editCompleted",
					"mouseup": "mouseup"
				};
				return _.extend(parentEvents, myEvents);
			},

			/**
			 * Initialize Image component view.
			 */
			initialize: function() {
				ComponentView.prototype.initialize.apply(this, arguments);
				if (this.model.get("imageType") === "SVG") {
					this.scale = Mixers.scaleByResize;
					this.model.off("change:scale", this._setUpdatedTransform, this);
					this.model.on("change:scale", Mixers.scaleChangeByResize, this);
				}

				this.dblclicked = this.dblclicked.bind(this);
				TouchBridge.on.dblclick(this.$el, this.dblclicked);
			},

			/**
			 * Render element based on component model.
			 *
			 * @returns {*}
			 */
			render: function() {
				var $img,
					_this = this;
				ComponentView.prototype.render.call(this);
				$img = $("<img src=" + (this.model.get('src')) + "></img>");
				$img.load(function() {
					return _this._finishRender($(this));
				});
				$img.error(function() {
					return _this.remove();
				});

				return this.$el;
			},

			dblclicked: function(e) {
				
				var linkModel = this.model.get('href');
				
				if(linkModel){
					if(linkModel.indexOf('#/step-')>=0)
						linkModel = linkModel.substr(7);
				}

				var link = prompt("Ingrese el numero de la diapositiva o un enlace externo (escriba 0 para eliminar el enlace)", linkModel);

				if (link != null && link > 0) {
				    this.model.set('href', '#/step-' + link );
				    this.model.set('newTab', false);
				}else if(link && (link.indexOf('http') >= 0 || link.indexOf('https') >= 0)){
					this.model.set('href', link);
					this.model.set('newTab', true);
				}else if(link==0){
					this.model.unset('href');
					this.model.unset('newTab');
				}
			},

			/**
			 * Do the actual rendering once image is loaded.
			 *
			 * @param {jQuery} $img
			 * @returns {*}
			 * @private
			 */
			_finishRender: function($img) {
				var height, naturalHeight, naturalWidth, scale, width;
				naturalWidth = $img[0].naturalWidth;
				naturalHeight = $img[0].naturalHeight;
				if (this.model.get("imageType") === "SVG") {
					$img.css({
						width: "100%",
						height: "100%"
					});
					scale = this.model.get("scale");
					if (scale && scale.width) {
						this.$el.css({
							width: scale.width,
							height: scale.height,
							x: 1,
							y: 1
						});
					} else {
						width = Math.max(naturalWidth, 50);
						height = Math.max(naturalHeight, 50);
						this.model.set("scale", {
							width: width,
							height: height,
							x: 1,
							y: 1
						});
					}
				} else {
					this.origSize = {
						width: naturalWidth,
						height: naturalHeight
					};
					$img[0].width = naturalWidth;
					$img[0].height = naturalHeight;
					this._setUpdatedTransform();
				}
				$img.bind("dragstart", function(e) {
					e.preventDefault();
					return false;
				});
				this.$content.append($img);
				if (this.model.get("imageType") === "SVG") {
					$img.parent().addClass("svg");
					return $img.parent().parent().addClass("svg");
				}
			}
		});
	});