define(["./ComponentView",
	"tantaman/web/interactions/TouchBridge",
	"./TooltipModal",
	"strut/slide_editor/view/Utils",
	"css!styles/slide_components/jquery.qtip.css"],
	function(ComponentView, TouchBridge, TooltipModal,OTUtils) {
		'use strict';

		/**
		 * @class TooltipView
		 * @augments ComponentView
		 */
		return ComponentView.extend({
			className: "component tooltipView",

			/**
			 * Initialize TooltipView component view.
			 */
			initialize: function() {
				ComponentView.prototype.initialize.apply(this, arguments);

				this.dblclicked = this.dblclicked.bind(this);
				TouchBridge.on.dblclick(this.$el, this.dblclicked);

				if(!this.model.get('dataTooltip')){
					var dataTooltip = { 
						id: 't'+Date.now(),
						position: {
							at: 'center right',
							my: 'center left'
						},
						show: { event: 'mouseenter' },
						hide: { event: 'mouseout' },
						content: { text: 'Tooltip', type: 'text' }
					};

					this.model.set('dataTooltip',dataTooltip);
				}
				
				if(typeof this.model.get('dataTooltip').show == "string" ){
					var dt = this.model.get('dataTooltip');
					dt.show = { event: dt.show };
					this.model.set('dataTooltip',dt);
				}

				if(typeof this.model.get('dataTooltip').hide == "string" ){
					var dt = this.model.get('dataTooltip');
					dt.hide = { event: dt.hide };
					this.model.set('dataTooltip',dt);
				}
				
				this.modal = new TooltipModal(this.model.get('dataTooltip'));
				this.modal.render();
			},

			/**
			 * Render element based on component model.
			 *
			 * @returns {*}
			 */
			render: function() {
				ComponentView.prototype.render.call(this);
				this._template = JST['strut.slide_components/TooltipHelper'](this.model);
				
				this.$tooltip = $(this._template);
				
				if(!this.model.getComponent())
					this.$tooltip.css('background','#e74c3c');

				this.$content.append(this.$tooltip);

				this.$el.find('[data-delta]').remove();

				return this.$el;
			},

			dblclicked: function(e) {
				var _this = this;
				this.modal.show( _this.model.get('dataTooltip') ,function (dataTooltip) {
					_this.model.set('dataTooltip',dataTooltip);
				});
			},

			/**
			 * Event: drag is in progress.
			 *
			 * @param {Event} e
			 */
			drag: function(e) {
				var dx, dy, newX, newY, components;
				if (this._dragging && this.allowDragging) {

					this.intersectedComponent = null;
					components = $('.component:not(.tooltipView)');
					
					components.removeClass('comp-intersected');
					
					for (var i = components.length - 1; i >= 0 ; i--) {
						var $component = $( components[i] );
						var collision = this.collision($component,e.pageX,e.pageY,this.$operatingTable);
						if(collision){
							this.intersectedComponent = $component.addClass('comp-intersected');
							i = -1;
						}
					};


					dx = e.pageX - this._prevMousePos.x;
					dy = e.pageY - this._prevMousePos.y;
					newX = this._prevPos.x + dx / this.dragScale;
					newY = this._prevPos.y + dy / this.dragScale;

					this.model.setInt("x", newX);
					this.model.setInt("y", newY);

					if (!this.dragStartLoc) {
						this.dragStartLoc = {
							x: newX,
							y: newY
						};
					}

				}
			},

			/**
			 * Event: drag has been stopped.
			 *
			 * @param {Event} e
			 */
			dragStop: function(e) {
				ComponentView.prototype.dragStop.apply(this, arguments);
				var intersectedModelComponent;

				$('.component:not(.tooltipView)').removeClass('comp-intersected');

				if(this.intersectedComponent != null)
					intersectedModelComponent = this._getModelIntersectedComponent( this.intersectedComponent );
				else if(this.intersectedComponent == null ){
					intersectedModelComponent = this.model.getComponent();
				}

				if(typeof(intersectedModelComponent) != 'undefined')
					this._attachToComponent( this.intersectedComponent, intersectedModelComponent );

			},

			_getModelIntersectedComponent: function($componentEl){
				var cid, modelIntersectedComponent;
				if(typeof($componentEl) == "undefined" || $componentEl == null)
					return;

				cid = $componentEl.data('cid');

				if(typeof(cid) == "undefined")
					return;

				modelIntersectedComponent = this.model.slide.get('components').filter(function(component){
					return component.cid == cid;
				});

				return (modelIntersectedComponent.length) ? modelIntersectedComponent[0] : undefined;
			},

			_attachToComponent : function ($componentView, componentModel){
				var pos, $compContent, _this;

				if(!this.model.get('scale').width || !this.model.get('scale').height){
					_this = this;

					var scale = {
						width: _this.$el.width(),
						height: _this.$el.height(),
						x: this.model.get('scale').x,
						y: this.model.get('scale').y
					};

					this.model.set('scale', scale);
				}

				if(!componentModel.get('scale').width || !componentModel.get('scale').height){

					var scale = {
						width: $componentView.width(),
						height: $componentView.height(),
						x: componentModel.get('scale').x,
						y: componentModel.get('scale').y
					};

					componentModel.set('scale', scale);
				}

				

				if( typeof(componentModel.get('dataTooltip')) == "undefined" ){
					this._dettachComponent();
					pos = this.model.setPosition(componentModel);
					this.$tooltip.css('background','');
				}else if( componentModel.get('dataTooltip').id == this.model.get('dataTooltip').id ){
					pos = this.model.setPosition(componentModel);
				}else if( componentModel.get('dataTooltip').id != this.model.get('dataTooltip').id ){
					$compContent = $componentView.find('.content-scale');
					this.model.setInt("x", $compContent.width() + componentModel.get('x') + 100 );
					this.model.setInt("y", componentModel.get('y') );
					console.log('Existe un tooltip asociado a este elemento, solo puedes agregar un tooltip por elemento');
					return;
				}

				if (!this.dragStartLoc) {
					this.dragStartLoc = {
						x: pos[0],
						y: pos[1]
					};
				}

				componentModel.set('dataTooltip',this.model.get('dataTooltip'));
			},

			_dettachComponent : function (){
				this.$tooltip.css('background','#e74c3c');
				var attachedComponent = this.model.getComponent();
				console.log(attachedComponent);
				if(attachedComponent)
					attachedComponent.unset('dataTooltip');
			},

			/**
			 * React on clicking X remove button.
			 *
			 * @param {Event} e
			 */
			removeClicked: function(e) {
				this._dettachComponent();
				ComponentView.prototype.removeClicked.apply(this, arguments);
			},

			collision: function($div1, mouseX, mouseY, $operatingTable) {
				var dim = OTUtils.computeSlideDimensions($operatingTable);
				var x1 = $div1.offset().left;
				var y1 = $div1.offset().top;
				var h1 = $div1.height() * dim.scale;
				var w1 = $div1.width() * dim.scale;
				var b1 = y1 + h1;
				var r1 = x1 + w1;
				var x2 = mouseX;
				var y2 = mouseY;
				var h2 = mouseX + 2;
				var w2 = mouseY + 2;
				var b2 = y2 + h2;
				var r2 = x2 + w2;

				if (b1 < y2 || y1 > b2 || r1 < x2 || x1 > r2) return false;
				return true;
		    },

		});
	});