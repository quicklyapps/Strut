define(function() {
    var mixers;
    return mixers = {
      scaleByResize: function(e, deltas) {
        var height, offset, width;
        offset = this.$el.offset();
        width = (deltas.x - offset.left) / this.dragScale;
        height = (deltas.y - offset.top) / this.dragScale;
        return this.model.set("scale", {
          width: width,
          height: height
        });
      },

      scaleChangeByResize: function(model, size) {
        if (this.origSize) {
          var factor = size.width / this.origSize.width;
          size.height = this.origSize.height * factor;
        }
        this.$el.css(size);
      },

      scaleObjectEmbed: function(e, deltas) {
        var scale;
 
        if( $(e.target).data('delta') == 'scale'){
          scale = {
            width: this._initialScale.width + deltas.dx,
            height: this._initialScale.height + deltas.dx
          };
        }else if( $(e.target).data('delta') == 'scalex'){
          scale = {
            width: this._initialScale.width + deltas.dx,
            height: this._initialScale.height
          };
        }else if( $(e.target).data('delta') == 'scaley'){
          scale = {
            width: this._initialScale.width,
            height: this._initialScale.height + deltas.dy
          };
        }

        scale.x = 1;
        scale.y = 1;
        
        return this.model.set("scale", scale);
      },

      scaleChangeObjectEmbed: function(model, size) {
          //this.$object.attr(size);
          //this.$embed.attr(size);
          this.$el.css(size);
      },

      scaleChangeInlineSvg: function(model, size) {
        if(this.$object){
          this.$object.attr(size);
          this.$el.css(size);
        }
      }
    };
  });