define(['libs/backbone',
	'tantaman/web/widgets/ManagedColorChooser',
	"css!styles/slide_components/handsontable.full.css",
	"css!styles/slide_components/handsontable-extension.css"],
function(Backbone, ManagedColorChooser) {
	/**
	* A modal that present a flat color chooser.
	*/

	var colorChooser = ManagedColorChooser.get('floatingChooserBorder');

	return Backbone.View.extend({
		className: 'modal hide tableModal',

		events: {
			'click .ok': '_apply',
			'click [data-border]': 'startSetCellsBorders',
			'click [data-borderWidth]': 'setTableBorderWidth',
			'click .borderColor': 'setTableBorderColor'
		},

		/**
		 * @class TableModal
		 */
		initialize: function(htData) {
			var _this = this;

			this.htData = htData;

			this.borderPos = null;
			this.borderStyle = 'solid';
			this.borderWidth = '1px';
			this.borderColor = 'black';

			this.options = {
				data: htData.data,
				rowHeaders: false,
				colHeaders: false,
				contextMenu: {
					items: _this.spanishMenu()
				},
    			mergeCells: (this.htData.mergeCells) ? this.htData.mergeCells: true,
    			minCols:1,
				minRows:1,
				manualColumnResize: true,
				manualRowResize: true,
				renderer: function(instance, TD, row, col, prop, value, cellProperties) {

					if( _this.htData.meta && _this.htData.meta[row][col]){
						if(_this.htData.meta[row][col]!= false){
							instance.setCellMetaObject(row, col, _this.htData.meta[row][col]);	
							_this.htData.meta[row][col] = false;
						}
					}

					var $TD = $(TD);

					if(cellProperties["border-top"])
						TD.style['border-top'] = cellProperties["border-top"];

					if(cellProperties["border-bottom"])
						TD.style['border-bottom'] = cellProperties["border-bottom"];

					if(cellProperties["border-left"])
						TD.style['border-left'] = cellProperties["border-left"];

					if(cellProperties["border-right"])
						TD.style['border-right'] = cellProperties["border-right"];

					if(value)
						TD.textContent=value;

					if(cellProperties.className)
						TD.className = cellProperties.className;
				},
				afterSelection: function(r,c,r2,c2){
					_this.selectedCells = [r,c,r2,c2];
				}
			};
		},
		show: function(changeTbProperties) {
			var _this = this;
			this.changeTbProperties = changeTbProperties;

			setTimeout(function () {
				_this.ht.render();	
			},100);


			this.$el.css('width', '96%');
			this._$body.css('min-height', '400px');
    		this.$el.css('margin', 'auto -48%');
			this.$el.modal('show');
		},

		hide: function() {
			this.$el.modal('hide');
		},

		_apply: function() {
			var $table = this._$tableCtn.find('.ht_master .wtSpreader');

			this.htData.meta = this.getTableMetaData();
			this.htData.data = this.ht.getData();
			this.htData.html = $table.html();

			if(this.ht.mergeCells.mergedCellInfoCollection.length)
				this.htData.mergeCells = this.ht.mergeCells.mergedCellInfoCollection;

			this.changeTbProperties( this.htData );
			this.hide();
		},

		render: function() {
			var self = this;
			this.$el.html( JST['strut.slide_components/TableModal'](self) );
			this._$body = this.$el.find('.modal-body');
			this._$tableCtn = this.$el.find('#table' + self.cid);

			this.ht = Handsontable(this._$tableCtn[0],this.options);
			
			return this;
		},

		startSetCellsBorders: function (e){
			if(this.selectedCells){
				this.borderPos = $(e.target).data('border');
				this.setCellsBorders(this.borderPos, this.borderWidth, this.borderStyle, this.borderColor);
				this.ht.selectCell(this.selectedCells[0], this.selectedCells[1], this.selectedCells[2], this.selectedCells[3], true);
			}
		},

		setCellsBorders: function (pos,width,style,color){
			var dataCells = this.selectedCells;

			if(!dataCells)
				return;

			var cells = [];
			var borderData = width + ' ' + style + ' ' + color;

			if(pos == "up" || pos == "outside"){
				cells = [];
				for (var i = dataCells[1]; i <= dataCells[3]; i++) {
					this.ht.setCellMeta( dataCells[0], i, 'border-top', borderData);
					cells.push( this.ht.getCell(dataCells[0],i) );
				};

				$(cells).css('border-top',borderData);
			}

			if(pos == "down" || pos == "outside"){
				cells = [];
				for (var i = dataCells[1]; i <= dataCells[3]; i++) {
					this.ht.setCellMeta( dataCells[2], i, 'border-bottom', borderData);
					cells.push( this.ht.getCell(dataCells[2],i) );
				};

				$(cells).css('border-bottom',borderData);
			}

			if(pos == "left" || pos == "outside"){
				cells = [];
				for (var i = dataCells[0]; i <= dataCells[2]; i++) {
					this.ht.setCellMeta( i,dataCells[1], 'border-left', borderData);
					cells.push( this.ht.getCell(i,dataCells[1]) );
				};

				$(cells).css('border-left',borderData);
			}

			if(pos == "right" || pos == "outside"){
				cells = [];
				for (var i = dataCells[0]; i <= dataCells[2]; i++) {
					this.ht.setCellMeta( i,dataCells[3], 'border-right', borderData);
					cells.push( this.ht.getCell(i,dataCells[3]) );
				};

				$(cells).css('border-right',borderData);
			}

			if(pos == "all"){
				var cells = [];

				for (var i = dataCells[0]; i <= dataCells[2]; i++) {
					for (var j = dataCells[1]; j <= dataCells[3]; j++) {
						this.ht.setCellMeta( i, j, 'border-top', borderData);
						this.ht.setCellMeta( i, j, 'border-bottom', borderData);
						this.ht.setCellMeta( i, j, 'border-left', borderData);
						this.ht.setCellMeta( i, j, 'border-right', borderData);
						cells.push( this.ht.getCell(i,j) );
					};
				};

				$( cells ).css('border',borderData);
			}

			if(pos == "remove"){
				var cells = [];

				for (var i = dataCells[0]; i <= dataCells[2]; i++) {
					for (var j = dataCells[1]; j <= dataCells[3]; j++) {
						this.ht.setCellMeta( i, j, 'border-top', '');
						this.ht.setCellMeta( i, j, 'border-bottom', '');
						this.ht.setCellMeta( i, j, 'border-left', '');
						this.ht.setCellMeta( i, j, 'border-right', '');
						cells.push( this.ht.getCell(i,j) );
					};
				};

				$( cells ).css('border','');
			}
		},

		setTableBorderWidth: function(e){
			var $badge = this._$body.find('[data-borderWidth="'+ this.borderWidth +'"] .badge');
			this.borderWidth = $(e.target).data('borderwidth');
			this._$body.find('[data-borderWidth="'+ this.borderWidth +'"]').prepend($badge);

			if(this.selectedCells){
				this.setCellsBorders(this.borderPos, this.borderWidth, this.borderStyle, this.borderColor);
				this.ht.selectCell(this.selectedCells[0], this.selectedCells[1], this.selectedCells[2], this.selectedCells[3], true);
			}			
		},

		setTableBorderColor: function(e){
			if(this.selectedCells)
				this.ht.selectCell(this.selectedCells[0], this.selectedCells[1], this.selectedCells[2], this.selectedCells[3], true);

			var _this = this;

			colorChooser.show({
				top: 0,
				left: 350,
				move: function(color) {
          			_this.borderColor = color.toHexString();
          			if(_this.selectedCells)
          				_this.setCellsBorders(_this.borderPos, _this.borderWidth, _this.borderStyle, _this.borderColor);
          		},
				appendTo: '.borderColor'
			});
		},

		getAllSelectedCells: function(dataCells){
			var cells = [];

			for (var i = dataCells[0]; i <= dataCells[2]; i++) {
				for (var j = dataCells[1]; j <= dataCells[3]; j++) {
					cells.push( this.ht.getCell(i,j) );
				};
			};

			return cells;
		},

		getTableMetaData: function () {
			var rows = this.ht.countRows();
			var cols = this.ht.countCols();
			var metaData = new Array(rows);

			for (var i = 0; i < rows; i++) {
				metaData[i] = new Array(cols);
				for (var j = 0; j < cols; j++) {
					var cellMetaData = this.ht.getCellMeta(i,j);
					cellMetaData.instance = null;
					metaData[i][j] = cellMetaData;
				};
			};

			return metaData;
		},

		getMergeCells: function () {
			var mergeInfo = this.ht.mergeCells.mergedCellInfoCollection;
			return  false;
		},

		spanishMenu : function () {
			var self = this;

			var items = {
				"row_above": {
					name: "Insertar fila arriba"
				},
				"row_below": {
					name: "Insertar fila abajo"
				},
				"col_left": {
					name: "Insertar columna a la izquierda"
				},
				"col_right": {
					name: "Insertar columna a la derecha"
				},
				"remove_row": {
					name: "Eliminar fila(s)"
				},
				"remove_col": {
					name: "Eliminar columna(s)"
				},
				"mergeCells": {
					name: "Unir/Separar celdas"
				},
				"alignment": {
					name: "Alineacion"
				}
			}

			return items;
		},

		constructor: function TableModal() {
			Backbone.View.prototype.constructor.apply(this, arguments);
		},
	});
});