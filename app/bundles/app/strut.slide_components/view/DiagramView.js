define(["./ComponentView",
	'tantaman/web/interactions/TouchBridge',
	"./DiagramModal",
	"css!styles/slide_components/joint.css",
	"css!styles/slide_components/joint-extension.css"],
	function(ComponentView, TouchBridge, DiagramModal) {

		/**
		 * @class mindMapView
		 * @augments ComponentView
		 */
		return ComponentView.extend({
			className: "component mindMapView",

			/**
			 * Initialize mindMapView component view.
			 */
			initialize: function() {
				ComponentView.prototype.initialize.apply(this, arguments);
				this.dblclicked = this.dblclicked.bind(this);
				TouchBridge.on.dblclick(this.$el, this.dblclicked);
				var dataDiagram = {
					data : null,
					html : "",
				};

				if(!this.model.get('dataDiagram'))
					this.model.set('dataDiagram',dataDiagram);

				this.modal = new DiagramModal( this.model.get('dataDiagram') );
			},
 
			/**
			 * Render element based on component model.
			 *
			 * @returns {*}
			 */
			render: function() {
				var $frame, scale;
				ComponentView.prototype.render.call(this);

				this.$content
					.html( this.model.get('dataDiagram').html )
					.addClass( 'diagramContainer' )
					.find('button.delete, .link-tools, .marker-arrowheads').remove();

				if( this.model.get('dataDiagram').html == "" ){
					this.$content.css({
						width:'512px',
						height: '384px'
					});
				}

				this.modal.render();
				
				return this.$el;
			},

			/**
			 * Event: element is double clicked. Enter editing mode for a textbox.
			 *
			 * @param {Event} e
			 */
			dblclicked: function(e) {
				var _this = this;
				this.modal.show(function(data){
					var $diagram = $("<div class='diagramContainer'>" + data.html + "</div>");
					$diagram.find('button.delete, .link-tools, .marker-arrowheads, .linker').remove();
					_this.$content.html($diagram.html()).addClass('diagramContainer');
					data.html = $diagram.html();
					_this.model.set('dataDiagram',data);
					console.log(_this.model);
				});
			}


		});
	});