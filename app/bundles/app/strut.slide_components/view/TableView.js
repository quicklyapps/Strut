define(['./ComponentView',
		'tantaman/web/interactions/TouchBridge',
		'./TableModal'],
function(ComponentView, TouchBridge, TableModal) {


	return ComponentView.extend({
		className: 'component tableView',


		initialize: function() {
			ComponentView.prototype.initialize.apply(this, arguments);
			this.dblclicked = this.dblclicked.bind(this);
			TouchBridge.on.dblclick(this.$el, this.dblclicked);


			if( !this.model.get("htData") ){

				var arrData = new Array(4);
				for (var i = 0; i < arrData.length; i++) {
					arrData[i] = new Array(4);
				};

				var htData = {
					data: arrData,
					html: '<table class="htCore"><colgroup><col style="width: 50px;"><col style="width: 50px;"><col style="width: 50px;"><col style="width: 50px;"></colgroup><thead></thead><tbody><tr><td class=""></td><td class=""></td><td class=""></td><td class=""></td></tr><tr><td class=""></td><td class=""></td><td class=""></td><td class=""></td></tr><tr><td class=""></td><td class=""></td><td class=""></td><td class=""></td></tr><tr><td class=""></td><td class=""></td><td class=""></td><td class=""></td></tr></tbody></table>'
				};

				this.model.set("htData", htData);
			}

			this.modal = new TableModal( this.model.get("htData") );
		},

		render: function() {
			ComponentView.prototype.render.call(this);

			this.$content.append( this.model.get("htData").html );
			this.$content.addClass('handsontable');
			this.$el.append('<div class="overlay"></div>');
			this.modal.render();
			
			return this.$el;
		},

		dblclicked: function(e) {
			var _this = this;
			this.modal.show(function (htData) {
				var scale;
				_this.model.set("htData", htData);
				_this.$content.html(htData.html);
				_this._setUpdatedTransform();
			});
			
		}

	});
});