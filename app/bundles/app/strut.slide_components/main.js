define(['./view/ComponentButton',
	'./view/ImportingComponentButton',
	'./model/Image',
	'./model/Diagram',
	'./model/Shape',
	'./model/Table',
	'./model/TextBox',
	'./model/Tooltip',
	'./model/Video',
	'./model/WebFrame',
	'./model/GeogebraFrame',
	'./view/ImageView',
	'./view/DiagramView',
	'./view/ShapeView',
	'./view/TableView',
	'./view/TextBoxView',
	'./view/TooltipView',
	'./view/VideoView',
	'./view/WebFrameView',
	'./view/GeogebraFrameView',
	'./ComponentFactory',
	'lang',
	'./view/ShapesDropdown',
	'./ShapeCollection'],
	function(Button,
			 ImportingComponentButton,
			 Image,
			 Diagram,
			 Shape,
			 Table,
			 TextBox,
			 Tooltip,
			 Video,
			 WebFrame,
			 GeogebraFrame,
			 ImageView,
			 DiagramView,
			 ShapeView,
			 TableView,
			 TextBoxView,
			 TooltipView,
			 VideoView,
			 WebFrameView,
			 GeogebraFrameView,
			 ComponentFactory,
			 lang,
			 ShapesDropdown,
			 ShapeCollection) {
		ShapeView
		var availableShapes = new ShapeCollection();

		var service = {
			createButtons: function(editorModel) {
				var buttons = [];

				buttons.push(new Button({
					componentType: 'TextBox',
					icon: 'icon-text-width',
					name: lang.text,
					editorModel: editorModel
				}));

				buttons.push(new ImportingComponentButton({
					componentType: 'Image',
					icon: 'icon-picture',
					name: lang.image,
					tag: 'img',
					title: lang.insert_image,
					editorModel: editorModel,
					browsable: true
				}));

				buttons.push(new ImportingComponentButton({
					componentType: 'Video',
					icon: 'icon-film',
					name: lang.video,
					tag: 'video',
					title: lang.insert_video,
					editorModel: editorModel,
					ignoreErrors: true
				}));

				buttons.push(new ImportingComponentButton({
					componentType: 'WebFrame',
					icon: 'icon-globe',
					name: lang.website,
					tag: 'iframe',
					title: lang.insert_website,
					editorModel: editorModel
				}));

				/*
				buttons.push(new ImportingComponentButton({
					componentType: 'GeogebraFrame',
					icon: 'icon-globe',
					name: 'Geogebra',
					tag: 'iframe',
					title: lang.insert_website,
					editorModel: editorModel
				}));
				*/


				buttons.push(new Button({
					componentType: 'GeogebraFrame',
					icon: 'icon-globe',
					name: 'Geogebra',
					editorModel: editorModel
				}));

				buttons.push(new Button({
					componentType: 'Table',
					icon: 'icon-list-alt',
					name: lang.table,
					editorModel: editorModel
				}));
				
				buttons.push(new Button({
					componentType: 'Diagram',
					icon: 'icon-star-empty',
					name: lang.diagram,
					editorModel: editorModel
				}));


				buttons.push(new Button({
					componentType: 'Tooltip',
					icon: 'icon-comment',
					name: lang.tooltip,
					editorModel: editorModel
				}));
				
				buttons.push(new ShapesDropdown(
					availableShapes,
					JST['strut.slide_components/ShapesDropdown'],
					{class: 'group-dropdown',
						editorModel: editorModel}
				));

				return buttons;
			}
		};

		return {
			initialize: function(registry) {
				// Register each component as a service
				// so it can be picked up by the ComponentFactory
				// If 3rd parties want to add components
				// then they just add their components to the registry as well.
				registry.register({
					interfaces: 'strut.ComponentButtonProvider'
				}, service);

				registry.register({
					interfaces: 'strut.ComponentModel',
					meta: {
						type: 'Image'
					}
				}, Image);

				registry.register({
					interfaces: 'strut.ComponentModel',
					meta: {
						type: 'TextBox'
					}
				}, TextBox);

				registry.register({
					interfaces: 'strut.ComponentModel',
					meta: {
						type: 'Table'
					}
				}, Table);

				registry.register({
					interfaces: 'strut.ComponentModel',
					meta: {
						type: 'WebFrame'
					}
				}, WebFrame);


				registry.register({
					interfaces: 'strut.ComponentModel',
					meta: {
						type: 'GeogebraFrame'
					}
				}, GeogebraFrame);

				registry.register({
					interfaces: 'strut.ComponentModel',
					meta: {
						type: 'Tooltip'
					}
				}, Tooltip);

				registry.register({
					interfaces: 'strut.ComponentModel',
					meta: {
						type: 'Diagram'
					}
				}, Diagram);

				registry.register({
					interfaces: 'strut.ComponentModel',
					meta: {
						type: 'Video'
					}
				}, Video);

				registry.register({
					interfaces: 'strut.ComponentModel',
					meta: {
						type: 'Shape'
					}
				}, Shape);

				registry.register({
					interfaces: 'strut.ComponentView',
					meta: {
						type: 'Image'
					}
				}, ImageView);

				registry.register({
					interfaces: 'strut.ComponentView',
					meta: {
						type: 'TextBox'
					}
				}, TextBoxView);

				registry.register({
					interfaces: 'strut.ComponentView',
					meta: {
						type: 'Table'
					}
				}, TableView);

				registry.register({
					interfaces: 'strut.ComponentView',
					meta: {
						type: 'Tooltip'
					}
				}, TooltipView);

				registry.register({
					interfaces: 'strut.ComponentView',
					meta: {
						type: 'Diagram'
					}
				}, DiagramView);

				registry.register({
					interfaces: 'strut.ComponentView',
					meta: {
						type: 'WebFrame'
					}
				}, WebFrameView);


				registry.register({
					interfaces: 'strut.ComponentView',
					meta: {
						type: 'GeogebraFrame'
					}
				}, GeogebraFrameView);

				registry.register({
					interfaces: 'strut.ComponentView',
					meta: {
						type: 'Video'
					}
				}, VideoView);


				registry.register({
					interfaces: 'strut.ComponentView',
					meta: {
						type: 'Shape'
					}
				}, ShapeView);

				ComponentFactory.initialize(registry);
			}
		};
	});
