define(['strut/deck/Component'],
	function(Component, FileUtils) {
		'use strict';

		/**
		 * @class Diagram
		 * @augments Component
		 */
		return Component.extend({
			initialize: function() {
				Component.prototype.initialize.apply(this, arguments);
				this.set('type', 'Diagram');
			},

			constructor: function Diagram(attrs) {
				Component.prototype.constructor.call(this, attrs);
			}
		});
	});