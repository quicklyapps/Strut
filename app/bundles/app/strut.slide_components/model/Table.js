define(['strut/deck/Component'],
function(Component) {
	'use strict';

	return Component.extend({
		initialize: function() {
			Component.prototype.initialize.apply(this, arguments);
			this.set('type', 'Table');
		},

		constructor: function Table(attrs) {
			Component.prototype.constructor.call(this, attrs);
		}
	});
});