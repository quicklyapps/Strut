define(['strut/deck/Component'],
	function(Component) {
		'use strict';

		/**
		 * @class Tooltip
		 * @augments Component
		 */
		return Component.extend({
			initialize: function() {
				Component.prototype.initialize.apply(this, arguments);
				this.set('type', 'Tooltip');
			},

			//posible positions at, my
				//top left, bottom right
				//top center, bottom center
				//top right,  bottom left
				//center left, center right
				//center right, center left
				//bottom left, top right
				//bottom center, top center
				//bottom right, top left


			setPosition: function (componentModel){
				var pos, thisWidth, thisHeight, newX, newY, compX, compY, compWidth, compHeight;
				pos = this.get('dataTooltip').position.at;
				compX = componentModel.get('x');
				compY = componentModel.get('y');
				compWidth = componentModel.get('scale').width;
				compHeight = componentModel.get('scale').height;
				thisWidth = this.get('scale').width;
				thisHeight = this.get('scale').height;

				if(pos == "top left"){
					newX = compX - thisWidth;
					newY = compY - thisHeight - 2;
				}else if(pos == "top center"){
					newX = compX + (compWidth/2) - (thisWidth/2);
					newY = compY - thisHeight - 2;
				}else if(pos == "top right"){
					newX = compX + compWidth;
					newY = compY - thisHeight + 2;
				}else if(pos == "center left"){
					newX = compX - thisWidth;
					newY = compY + (compHeight/2) - (thisHeight/2) + 2;
				}else if(pos == "center right"){
					newX = compX + compWidth + 2;
					newY = compY + (compHeight/2) - (thisHeight/2) + 2;
				}else if(pos == "bottom left"){
					newX = compX - thisWidth;
					newY = compY + compHeight + 2;
				}else if(pos == "bottom center"){
					newX = compX + (compWidth/2) - (thisWidth/2);
					newY = compY + compHeight + 2;
				}else if(pos == "bottom right"){
					newX = compX + thisWidth + 2;
					newY = compY + compHeight + 2;
				}else{
					newX = 0;
					newY = 0;
				}

				this.setInt("x", newX);
				this.setInt("y", newY);
				
				return [newX, newY];
			},

			getComponent: function(){
				var modelComponent, _this;

				_this = this;

				modelComponent = this.slide.get('components').filter(function(comp){
					return (typeof(comp.get('dataTooltip')) != 'undefined' && comp.get('dataTooltip').id == _this.get('dataTooltip').id && comp.get('type') != 'Tooltip');
				});

				return modelComponent[0] || undefined;
			},

			constructor: function Tooltip(attrs) {
				Component.prototype.constructor.call(this, attrs);
			}
		});
	});