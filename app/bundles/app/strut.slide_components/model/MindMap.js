define(['strut/deck/Component'],
	function(Component, FileUtils) {
		'use strict';

		/**
		 * @class MindMap
		 * @augments Component
		 */
		return Component.extend({
			initialize: function() {
				Component.prototype.initialize.apply(this, arguments);
				this.set('type', 'MindMap');
			},

			constructor: function MindMap(attrs) {
				Component.prototype.constructor.call(this, attrs);
			}
		});
	});