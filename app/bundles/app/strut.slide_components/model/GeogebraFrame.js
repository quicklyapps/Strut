define(['strut/deck/Component',
	'common/FileUtils'],
	function(Component, FileUtils) {
		'use strict';

		/**
		 * @class WebFrame
		 * @augments Component
		 */
		return Component.extend({
			initialize: function() {
				Component.prototype.initialize.apply(this, arguments);
				this.set('type', 'GeogebraFrame');
			},

			constructor: function GeogebraFrame(attrs) {
				Component.prototype.constructor.call(this, attrs);
			}
		});
	});