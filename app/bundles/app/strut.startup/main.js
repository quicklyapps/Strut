define(['strut/editor/EditorView',
        'strut/editor/EditorModel'],
function(EditorView, EditorModel) {
	var registry = null;
	var editorStartup = {
		run: function() {
			
			if(location.href.indexOf("http://localhost:9000/") == -1 ){
    			function getUrlParameter(sParam) {
	              	var sPageURL = decodeURIComponent(window.location.search.substring(1)),
				    sURLVariables = sPageURL.split('&'),
				    sParameterName,
				    i;

					for (i = 0; i < sURLVariables.length; i++) {
					    sParameterName = sURLVariables[i].split('=');
					    if (sParameterName[0] === sParam) {
					        return sParameterName[1] === undefined ? true : sParameterName[1];
					    }
					}
				}

				var id_pizarra = getUrlParameter("id_pizarra")
				console.log(id_pizarra)
				$.ajax({
				    method: "POST",
				    url: "/pizarra-multimedia/pizarra_consultar",
				    data: {
				        id_pizarra: id_pizarra
				    },
				}).done(function(data) {
				    console.log(data)
				    
				    if (data.error) {
				        alert(data.error)
				        return window.history.back()
				    }

				    var model = new EditorModel(registry);
				    var editor = new EditorView({ model: model, registry: registry });
				    editor.render();
				    $('body').append(editor.$el);
				    //localStorage.pizarra= JSON.stringify({"slides":[{"components":[],"z":0,"impScale":3,"rotateX":0,"rotateY":0,"rotateZ":0,"index":0,"selected":true,"active":true}],"background":"bg-default","activeSlide":{"components":[],"z":0,"impScale":3,"rotateX":0,"rotateY":0,"rotateZ":0,"index":0,"selected":true,"active":true}});

				    var pres = JSON.parse(data.datos);
				    model.importPresentation(pres);
				}).fail(function(data) {
				    alert("No se pudo conectar con el servidor");
				});

	    	}else{
	    		var model = new EditorModel(registry);
	    		var editor = new EditorView({model: model, registry: registry});
	    		editor.render();
	    		$('body').append(editor.$el);

	    		if (sessionMeta.lastPresentation != null) {
	    			// Load it up.
	    			var storageInterface = registry.getBest('strut.StorageInterface');
	    			storageInterface.load(sessionMeta.lastPresentation, function(pres, err) {
	    				if (!err) {
	    					model.importPresentation(pres);
	    				} else {
	    					console.log(err);
	    					console.log(err.stack);
	    				}
	    			});
	    		}
    		}
    		
		}
	};

	var welcome = {
		run: function() {
			// If no previous presentation was detected, show the welcome screen.
		}
	};

	return {
		initialize: function(reg) {
			registry = reg;
			registry.register({
				interfaces: 'strut.StartupTask'
			}, editorStartup);

			registry.register({
				interfaces: 'strut.StartupTask'
			}, welcome);
		}
	};
});