define(['css!styles/strut.themes/backgroundClasses.css'], function() {

	var generateBg = [
	// TODO: translate descriptions
		{klass: 'bg-solid-black', description: 'Black'},
		{klass: 'bg-solid-light', description: 'Light'},
		{klass: 'bg-solid-smoke', description: 'Smoke'}
	];

	for (var i = 1; i <= 20; i++) {
		generateBg.push({klass: 'bg-solid-'+i, description: 'BgSolid' + i });
	};

	return {
		title: 'background',
		backgrounds: generateBg
	}
});