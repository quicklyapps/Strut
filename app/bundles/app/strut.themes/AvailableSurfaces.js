define(['css!styles/strut.themes/surfaceClasses.css'], function() {

	var surfaces = [
			{klass: 'bg-surf-grad-black'},
			{klass: 'bg-surf-grad-light'},
			{klass: 'bg-surf-grad-smoke'}
		];

	for (var i = 1; i <= 20; i++) {
		surfaces.push({klass: 'bg-solid-'+i, description: 'BgSolid' + i });
	};

	return {
		title: 'surface',
		backgrounds: surfaces
	}
});