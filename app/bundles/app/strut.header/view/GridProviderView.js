define(['libs/backbone', '../model/GridProviderCollection'],
function(Backbone, GridProviderCollection) {
	return Backbone.View.extend({
		className: 'gridProviders',

		initialize: function(editorModel) {
			this._providerCollection = new GridProviderCollection(editorModel, {overflow: false});

			this._providerCollection.on('change:activeProviders', this.render, this);
		},

		render: function() {
			this.$el.empty();

			this._providerCollection.activeProviders().forEach(function(provider) {
				this.$el.append(provider.view().render().$el);
			}, this);

			return this;
		},

		constructor: function GridProviderView(editorModel) {
			Backbone.View.prototype.constructor.call(this, editorModel);
		}
	});
});