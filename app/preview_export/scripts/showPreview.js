var tpApis=[];
$( document ).ready(function() {
  if ($('#launched-placeholder'))
        loadPresentation();

    $("font").parents('div.componentContainer').css('width','');
    $("font").parents('div.antialias').css('display','').css('vertical-align','');
    $("font").parents('div.transformContainer').css('display','');
    $("div.antialias div span").css('line-height','');
    $(".antialias li *").css('line-height','');

    if (!window.presStarted) {
        startPres(document, window);
        impress().init();
    }

    if ("ontouchstart" in document.documentElement) { 
        $(".hint").html("<p>Toque a la izquierda o derecha para navegar</p>");
    }

    document.addEventListener("impress:stepenter",function (event) {
        console.log(event);
    },false);

    document.addEventListener("impress:stepleave",function (event) {
        console.log(event);
        tpApis.forEach(function(tpapi){
            if(typeof(tpapi) != "undefined")
                tpapi.hide();
        });
    },false);
    
    var tooltips = $('[data-tooltip]');
    

    for (var i = tooltips.length - 1; i >= 0; i--) {
        var $tooltip = $(tooltips[i]);
        var tooltipData = $tooltip.data('tooltip');
        
        if($tooltip.find('svg').length){
            if($tooltip.find('.overlay').length)
                $tooltip = $tooltip.find('.overlay');
        }

        if(tooltipData.content.type == 'text'){
            $tooltip.qtip(tooltipData);
        }else if(tooltipData.content.type == 'web'){
            tooltipData.content.text = '<iframe src="'+ tooltipData.content.text +'"></iframe>';
            tooltipData.hide.delay = 90;
            tooltipData.hide.event = 'unfocus';
            tooltipData.hide.fixed = true;
            tooltipData.style = { classes: 'qtip-iframe' };
            tooltipData.position.container = $tooltip.parents('.step');
            console.log($tooltip.parents('.step'));
            $tooltip.qtip(tooltipData).addClass('qtip-iframe');
        }else if(tooltipData.content.type == 'youtube'){
            console.log($tooltip.parents('.step'));
            tooltipData.position.container = $tooltip.parents('.step');
            $tooltip.qtip({ // Grab some elements to apply the tooltip to
                
                content: $('<div />', { id: tooltipData.content.text }),
                    position: tooltipData.position,
                    show: {
                        event: tooltipData.show.event,
                        effect: function() {
                            var style = this[0].style;
                            style.display = 'none';
                            setTimeout(function() { style.display = 'block'; }, 1);
                        }
                    },
                    hide: 'unfocus',
                    style: {
                        classes: 'qtip-youtube',
                        width: 275
                    },
                    events: {
                        render: function(event, api) {
                            new YT.Player(tooltipData.content.text, {
                                playerVars: {
                                    autoplay: 1,
                                    enablejsapi: 1,
                                    controls: 1,
                                    loop: 1,
                                    disablekb: 1,
                                    iv_load_policy: 3,
                                    rel: 0,
                                    showinfo: 0,
                                    modestbranding: 1,
                                    origin: document.location.host
                                },
                                origin: document.location.host,
                                height: 180,
                                width: 275,
                                videoId: tooltipData.content.text,
                                events: {
                                    'onReady': function(e) {
                                        api.player = e.target;
                                    },
                                }
                            });
                        },
                        hide: function(event, api){
                            api.player && api.player.stopVideo();
                        },
                        show: function(event, api){
                            api.player && api.player.playVideo();
                        }
                    },
            });
        }
        
        var api = $tooltip.qtip('api');
        console.log(api);
        tpApis.push( api );
    };
});