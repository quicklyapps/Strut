
var loadPresentation = function() {
	var presentation = localStorage.getItem('preview-string');
	var config = JSON.parse(localStorage.getItem('preview-config'));

	if (presentation) {
		document.body.innerHTML = presentation;
	//	document.body.className = config.surface + " " + document.body.className;
	}
};
/*
var loadPresentation = function(fn) {
	var id_pizarra = localStorage.getItem("id_pizarra");
	
	$.post("/pizarra_mostrar", {id_pizarra: id_pizarra}, function(pizarra){
		var presentation = pizarra.html;
		var config = JSON.stringify({});	
		if (presentation) {
			document.body.innerHTML = presentation;
			//fn();
		//	document.body.className = config.surface + " " + document.body.className;
		}
    });
	
};
*/
